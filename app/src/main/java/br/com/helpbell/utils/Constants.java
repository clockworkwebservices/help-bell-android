package br.com.helpbell.utils;

public class Constants {

  public static final String EXTRA_USER_EMAIL            = "userEmail";
  public static final String EXTRA_USER_PASSWORD         = "userPassword";

  public static final String EXTRA_SSID                  = "ssid";
  public static final String EXTRA_PASSWORD              = "password";
  public static final String EXTRA_MESSAGE               = "message";
  public static final String EXTRA_TITLE                 = "title";
  public static final String EXTRA_POSITIVE_BUTTON_LABEL = "positiveButtonLabel";
  public static final String EXTRA_NEGATIVE_BUTTON_LABEL = "negativeButtonLabel";
  public static final String EXTRA_REQUEST_CODE          = "requestCode";
  public static final String EXTRA_DEVICE_ID             = "deviceId";

  public static final int DIRECT_BLINKUP_REQUEST_CODE = 5;

  public static final int REQUEST_CODE_SIGN_IN      = 1111;
  public static final int REQUEST_CODE_NEW_PASSWORD = 2222;
  public static final int REQUEST_CODE_CONFIG       = 3333;
  public static final int REQUEST_CODE_WAKE_UP      = 4444;
  public static final int REQUEST_CODE_BLINK_UP     = 5555;

}
