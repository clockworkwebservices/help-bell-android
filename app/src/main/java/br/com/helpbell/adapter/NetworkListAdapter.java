package br.com.helpbell.adapter;

import android.content.Context;
import android.net.wifi.ScanResult;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.com.helpbell.R;

public class NetworkListAdapter extends BaseAdapter {

  private static final String TAG = NetworkListAdapter.class.getSimpleName();


  private List<ScanResult> mResults;
  private LayoutInflater inflater = null;

  public NetworkListAdapter(Context context, List<ScanResult> results) {
    Collections.sort(results, new Comparator<ScanResult>() {
      public int compare(ScanResult one, ScanResult two) {
        return one.SSID.compareToIgnoreCase(two.SSID);
      }
    });

    mResults = results;
    this.inflater  = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
  }

  @Override
  public int getCount() {
    return mResults.size();
  }

  @Override
  public Object getItem(int position) {
    return mResults.get(position);
  }

  @Override
  public long getItemId(int position) {
    return position;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    ScanResult result = mResults.get(position);
    Log.d(TAG, "Getting view to scan result: " + result.SSID + " - " + result.level);
    View vi = convertView;

    if (vi == null) {
      vi = inflater.inflate(R.layout.network_list_item, null);
    }
    TextView text = (TextView) vi.findViewById(R.id.item_text);
    text.setText(result.SSID);
    return vi;
  }

}
