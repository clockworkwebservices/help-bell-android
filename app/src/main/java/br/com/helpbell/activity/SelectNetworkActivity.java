package br.com.helpbell.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

import br.com.helpbell.R;
import br.com.helpbell.adapter.NetworkListAdapter;
import br.com.helpbell.persistance.UserManager;
import br.com.helpbell.utils.Constants;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class SelectNetworkActivity
  extends BaseActivity implements View.OnClickListener {

  private static final String TAG = SelectNetworkActivity.class.getSimpleName();

  @InjectView(R.id.wifi_list_view) ListView mListView;

  WifiManager mWifiManager;

  private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      String action = intent.getAction();
      Log.d(TAG, "Broadcast Received - " + action);

      switch (action) {
        case WifiManager.SCAN_RESULTS_AVAILABLE_ACTION:
          onWifiScanResult(mWifiManager.getScanResults());
          break;
      }
    }
  };

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_select_network);
    ButterKnife.inject(this);

    mListView.setOnItemClickListener(new ListItemClickListener());
  }

  @Override
  protected void onResume() {
    super.onResume();

    mWifiManager = (WifiManager)getSystemService(Context.WIFI_SERVICE);
    registerReceiver(mReceiver, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
    mWifiManager.startScan();
  }

  @Override
  protected void onPause() {
    super.onPause();

    unregisterReceiver(mReceiver);
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    Log.d(TAG, "onDestroy");
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
        finish();
        return true;
    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  @OnClick(R.id.choose_another_network_button)
  public void onClick(View v) {
    switch (v.getId()) {
      case R.id.choose_another_network_button:
        Intent intent = new Intent(this, AnotherNetworkActivity.class);
        startActivity(intent);
        break;
    }
  }

  private void onWifiScanResult(List<ScanResult> results) {
    Log.d(TAG, "Scan Results - " + results.size());
    mListView.setAdapter(new NetworkListAdapter(this, results));
  }

  private class ListItemClickListener implements ListView.OnItemClickListener {
    @Override
    public void onItemClick(AdapterView parent, View view, int position, long id) {
      mListView.setItemChecked(position, true);
      onListItemClicked(position);
    }
  }

  private void onListItemClicked(int position) {
    Intent intent = new Intent(this, NetworkPasswordActivity.class);
    ScanResult result = (ScanResult) mListView.getItemAtPosition(position);
    intent.putExtra(Constants.EXTRA_SSID, result.SSID);
    startActivity(intent);
  }

}
