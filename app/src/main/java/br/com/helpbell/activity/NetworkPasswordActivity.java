package br.com.helpbell.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Selection;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;

import br.com.helpbell.R;
import br.com.helpbell.utils.Constants;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class NetworkPasswordActivity extends BlinkUpActivity implements View.OnClickListener {

  private static final String TAG = NetworkPasswordActivity.class.getSimpleName();

  @InjectView(R.id.password) EditText mPasswordView;
  @InjectView(R.id.remember_password) Switch mRememberPasswordSwitch;

  private String mSsid;
  private boolean mRememberPassword;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_network_password);
    ButterKnife.inject(this);

    Intent intent = getIntent();
    mSsid = intent.getStringExtra(Constants.EXTRA_SSID);
    getSupportActionBar().setTitle(mSsid);

    String wifiPassword = getWifiPassword(mSsid);

    if (TextUtils.isEmpty(wifiPassword)) {
      mRememberPasswordSwitch.setChecked(false);
    } else {
      mRememberPasswordSwitch.setChecked(true);
      mPasswordView.setText(wifiPassword);
      int position = mPasswordView.length();
      Selection.setSelection(mPasswordView.getText(), position);
    }

    mRememberPasswordSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        mRememberPassword = isChecked;
      }
    });
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    Log.d(TAG, "onDestroy");
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
        finish();
        return true;
    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  @OnClick({ R.id.wake_up_device_button })
  public void onClick(View v) {
    switch (v.getId()) {
      case R.id.wake_up_device_button:
        wakeUpDevice();
        break;
    }
  }

  private void wakeUpDevice() {
    // Reset errors.
    mPasswordView.setError(null);
    boolean cancel = false;
    View focusView = null;
    String password = mPasswordView.getText().toString();

    if (TextUtils.isEmpty(password)) {
      mPasswordView.setError(getString(R.string.error_network_password_required));
      focusView = mPasswordView;
      cancel = true;
    }

    if (cancel) {
      // There was an error; don't attempt login and focus the first
      // form field with an error.
      focusView.requestFocus();
    } else {
      if (mRememberPassword) {
        saveWifiPassword(mSsid, password);
      } else {
        removeWifiPassword(mSsid);
      }
      blinkUp(mSsid, password);
    }
  }

}
