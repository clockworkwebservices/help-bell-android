package br.com.helpbell.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import br.com.helpbell.R;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class AnotherNetworkActivity extends BlinkUpActivity implements View.OnClickListener {

  private static final String TAG = AnotherNetworkActivity.class.getSimpleName();

  @InjectView(R.id.network_name) EditText mNetworkNameView;
  @InjectView(R.id.password) EditText mPasswordView;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_another_network);
    ButterKnife.inject(this);
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    Log.d(TAG, "onDestroy");
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
        finish();
        return true;
    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  @OnClick({ R.id.wake_up_device_button })
  public void onClick(View v) {
    switch (v.getId()) {
      case R.id.wake_up_device_button:
        wakeUpDevice();
        break;
    }
  }

  private void wakeUpDevice() {
    // Reset errors
    mNetworkNameView.setError(null);
    mPasswordView.setError(null);
    boolean cancel = false;
    View focusView = null;
    String networkName = mNetworkNameView.getText().toString();
    String password = mPasswordView.getText().toString();

    if (TextUtils.isEmpty(networkName)) {
      mNetworkNameView.setError(getString(R.string.error_network_name_required));
      focusView = mNetworkNameView;
      cancel = true;
    } else if (TextUtils.isEmpty(password)) {
      mPasswordView.setError(getString(R.string.error_network_password_required));
      focusView = mPasswordView;
      cancel = true;
    }

    if (cancel) {
      // There was an error; don't attempt login and focus the first
      // form field with an error.
      focusView.requestFocus();
    } else {
      blinkUp(networkName, password);
    }
  }

}
