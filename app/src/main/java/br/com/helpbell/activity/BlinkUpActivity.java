package br.com.helpbell.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.electricimp.blinkup.BlinkupController;

import br.com.helpbell.R;
import br.com.helpbell.persistance.NetworkManager;
import br.com.helpbell.utils.Constants;

public class BlinkUpActivity extends BaseActivity {

  private static final String TAG = BlinkUpActivity.class.getSimpleName();

  private static final String API_KEY = "deecc5c89100c8cd8835f0a298dd7543";

  protected BlinkupController mBlinkUpCtrl;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    mBlinkUpCtrl = BlinkupController.getInstance();
    mBlinkUpCtrl.setPlanID("8047ebd6e342eb55");
    mBlinkUpCtrl.stringIdCountdownDesc = getString(R.string.blink_up_count_down_desc);
    mBlinkUpCtrl.intentBlinkupComplete = new Intent(this, BlinkUpCompleteActivity.class);
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    Log.d(TAG, "onActivityResult - Request code: " + requestCode);
    Log.d(TAG, "onActivityResult - Result code: " + resultCode);

    switch (requestCode) {
      case Constants.DIRECT_BLINKUP_REQUEST_CODE:
        if (resultCode == RESULT_OK) {
          mBlinkUpCtrl.handleActivityResult(this, requestCode, resultCode, data);
        }
        break;
    }
  }

  protected void saveWifiPassword(String ssid, String password) {
    Log.d(TAG, "saveWifiPassword - " + ssid + " - " + password);
    NetworkManager manager = NetworkManager.getInstance(this);
    manager.saveWiFiPassword(ssid, password);
  }

  protected String getWifiPassword(String ssid) {
    NetworkManager manager = NetworkManager.getInstance(this);
    return manager.getWifiPassword(ssid);
  }

  protected void removeWifiPassword(String ssid) {
    NetworkManager manager = NetworkManager.getInstance(this);
    manager.removeWifiPassword(ssid);
  }

  protected void blinkUp(String ssid, String password) {
    mBlinkUpCtrl.setupDevice(this, ssid, password, API_KEY, true, errorHandler);
  }

  private BlinkupController.ServerErrorHandler errorHandler = new BlinkupController.ServerErrorHandler() {
    @Override
    public void onError(String errorMsg) {
      Toast.makeText(BlinkUpActivity.this, errorMsg, Toast.LENGTH_SHORT).show();
    }
  };

}
