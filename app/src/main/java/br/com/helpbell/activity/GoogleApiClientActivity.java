package br.com.helpbell.activity;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

public class GoogleApiClientActivity extends BaseActivity {

  private static final String TAG = GoogleApiClientActivity.class.getSimpleName();

  protected GoogleApiClient mGoogleApiClient;

  protected void onStart() {
    connectToGoogleApiClient(this);
    super.onStart();
  }

  protected void onStop() {
    if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
      mGoogleApiClient.disconnect();
    }
    super.onStop();
  }

  private void connectToGoogleApiClient(Context context) {
    if (mGoogleApiClient == null) {
      mGoogleApiClient = this.buildGoogleApiClient(context);
    }
    if (!mGoogleApiClient.isConnected() && !mGoogleApiClient.isConnecting()) {
      mGoogleApiClient.connect();
    }
  }

  protected Location getLastLocation() {
    Location location = null;

    if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
      if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
        // TODO: Consider calling
        //    ActivityCompat#requestPermissions
        // here to request the missing permissions, and then overriding
        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
        //                                          int[] grantResults)
        // to handle the case where the user grants the permission. See the documentation
        // for ActivityCompat#requestPermissions for more details.
        return location;
      }
      location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
    }
    return location;
  }

  protected void onGoogleApiClientConnected() {
  }

  private synchronized GoogleApiClient buildGoogleApiClient(Context context) {
    return new GoogleApiClient.Builder(context)
      .addConnectionCallbacks(new GoogleApiClientConnectionCallbacks())
      .addOnConnectionFailedListener(new GoogleApiClientOnConnectionFailedListener())
      .addApi(LocationServices.API)
      .build();
  }

  private class GoogleApiClientConnectionCallbacks implements GoogleApiClient.ConnectionCallbacks {

    @Override
    public void onConnected(Bundle bundle) {
      Log.d(TAG, "GoogleApiClient.onConnected");
      GoogleApiClientActivity.this.onGoogleApiClientConnected();
    }

    @Override
    public void onConnectionSuspended(int i) {
      Log.d(TAG, "GoogleApiClient.onConnectionSuspended");
    }
  }

  private class GoogleApiClientOnConnectionFailedListener implements GoogleApiClient.OnConnectionFailedListener {

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
      Log.d(TAG, "GoogleApiClient.onConnectionFailed");
      mGoogleApiClient = null;
    }

  }

}
