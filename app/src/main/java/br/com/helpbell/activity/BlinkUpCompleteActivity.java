package br.com.helpbell.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.electricimp.blinkup.BlinkupController;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Locale;

import br.com.helpbell.R;
import br.com.helpbell.model.Device;
import br.com.helpbell.persistance.DeviceManager;
import br.com.helpbell.utils.Constants;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class BlinkUpCompleteActivity extends BaseActivity implements View.OnClickListener {

  private static final String TAG = BlinkUpCompleteActivity.class.getSimpleName();

  private BlinkupController mBlinkUpController;

  @InjectView(R.id.waiting_wake_up_view) View waitingView;
  @InjectView(R.id.wake_up_result_view) View resultView;

  private BlinkupController.TokenStatusCallback mCallback = new BlinkupController.TokenStatusCallback() {

    @Override
    public void onSuccess(JSONObject json) {
      onWakeUpSuccess(json);
    }

    public void onError(String errorMsg) {
      onWakeUpError(errorMsg);
    }

    @Override
    public void onTimeout() {
      onWakeUpTimeout();
    }

  };

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_blink_up_complete);
    ButterKnife.inject(this);

    getSupportActionBar().hide();

    mBlinkUpController = BlinkupController.getInstance();
  }


  @Override
  protected void onResume() {
    super.onResume();
    mBlinkUpController.getTokenStatus(mCallback);
  }


  @Override
  protected void onPause() {
    super.onPause();
    mBlinkUpController.cancelTokenStatusPolling();
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    Log.d(TAG, "onDestroy");
  }

  @Override
  @OnClick({ R.id.try_again_button, R.id.cancel_button })
  public void onClick(View v) {
    Intent intent;

    switch (v.getId()) {
      case R.id.try_again_button:
        intent = new Intent(this, SelectNetworkActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        break;
      case R.id.cancel_button:
        intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        break;
    }
  }

  public void onWakeUpSuccess(JSONObject json) {
    try {
      Device device = Device.fromJsonObject(json);
      DeviceManager manager = DeviceManager.getInstance(BlinkUpCompleteActivity.this);
      manager.saveDevice(device);
//        ActivateDeviceService.start(this, device.id);

      Intent intent = new Intent(this, DeviceFoundActivity.class);
      intent.putExtra(Constants.EXTRA_DEVICE_ID, device.id);
      startActivity(intent);
    } catch (JSONException e) {
      onWakeUpError(e.getMessage());
    }
  }

  public void onWakeUpError(String errorMsg) {
    Log.w(TAG, "Blink failure - " + errorMsg);
    waitingView.setVisibility(View.GONE);
    resultView.setVisibility(View.VISIBLE);
  }

  public void onWakeUpTimeout() {
    Log.w(TAG, "Blink timeout");
    waitingView.setVisibility(View.GONE);
    resultView.setVisibility(View.VISIBLE);
  }

}
