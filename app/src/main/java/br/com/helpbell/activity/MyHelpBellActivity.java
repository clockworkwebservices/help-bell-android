package br.com.helpbell.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import br.com.helpbell.R;
import br.com.helpbell.utils.Constants;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MyHelpBellActivity extends BaseActivity implements View.OnClickListener {

  private static final String TAG = MyHelpBellActivity.class.getSimpleName();

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_my_help_bell);
    ButterKnife.inject(this);
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    Log.d(TAG, "onDestroy");
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
        finish();
        return true;
    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  @OnClick({ R.id.wake_up_device_button })
  public void onClick(View v) {
    switch (v.getId()) {
      case R.id.wake_up_device_button:
        startActivityForResult(new Intent(this, SelectNetworkActivity.class), Constants.REQUEST_CODE_WAKE_UP);
        break;
    }
  }

}
