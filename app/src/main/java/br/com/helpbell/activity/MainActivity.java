package br.com.helpbell.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import br.com.helpbell.R;
import br.com.helpbell.persistance.UserManager;
import br.com.helpbell.service.AuthenticationService;
import br.com.helpbell.utils.Constants;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity
  extends GoogleApiClientActivity
  implements OnMapReadyCallback, View.OnClickListener {

  private static final String TAG = MainActivity.class.getSimpleName();

  private GoogleMap mMap;
  private MapFragment mMapFragment;
  private Menu mMenu;

  BroadcastReceiver mReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      String action = intent.getAction();
      Log.d(TAG, "Broadcast Received - " + action);

      switch (action) {
        case AuthenticationService.ON_SIGN_OUT_SUCCESS:
          onSignOut();
          break;
        case AuthenticationService.ON_SIGN_OUT_FAILURE:
          break;
      }
    }
  };

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    ButterKnife.inject(this);
    mMapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map_fragment);
    mMapFragment.getMapAsync(this);
  }

  @Override
  protected void onResume() {
    super.onResume();
    IntentFilter intentFilter = new IntentFilter();
    intentFilter.addAction(AuthenticationService.ON_SIGN_OUT_SUCCESS);
    intentFilter.addAction(AuthenticationService.ON_SIGN_OUT_FAILURE);

    LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver, intentFilter);
  }

  @Override
  protected void onPause() {
    super.onPause();
    LocalBroadcastManager.getInstance(this).unregisterReceiver(mReceiver);
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    Log.d(TAG, "onActivityResult - " + requestCode + " - " + resultCode);

    switch (requestCode) {
      case Constants.REQUEST_CODE_SIGN_IN:
        if (resultCode == RESULT_OK) {
          onSignIn();
        }
        break;
      case Constants.REQUEST_CODE_CONFIG:
        if (resultCode == RESULT_OK) {
        }
        break;
    }
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    UserManager manager = UserManager.getInstance(this);
    getMenuInflater().inflate(R.menu.activity_main_authentication, menu);
    mMenu = menu;
    updateMenuOptions();
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.action_sign_in:
        signIn();
        return true;

      case R.id.action_sign_out:
        signOut();
        return true;

      default:
        return super.onOptionsItemSelected(item);

    }
  }

  @Override
  @OnClick({ R.id.button_my_helpbell, R.id.button_how_it_works })
  public void onClick(View v) {
    switch (v.getId()) {
      case R.id.button_how_it_works:
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://helpbell.zendesk.com"));
        startActivity(browserIntent);
        break;
      case R.id.button_my_helpbell:
        UserManager manager = UserManager.getInstance(this);

        if (manager.isAuthenticated()) {
          startActivityForResult(new Intent(this, MyHelpBellActivity.class), Constants.REQUEST_CODE_CONFIG);
        } else {
          signIn();
        }
        break;
    }
  }

  @Override
  public void onMapReady(GoogleMap googleMap) {
    mMap = googleMap;
  }

  protected void onGoogleApiClientConnected() {
    Location location = getLastLocation();

    if (location != null && mMap != null) {
      LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
      mMap.addMarker(new MarkerOptions().position(latLng));
      mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 11));
    }
  }

  private void signIn() {
    startActivityForResult(new Intent(this, LoginActivity.class), Constants.REQUEST_CODE_SIGN_IN);
  }

  private void onSignIn() {
    String text = getResources().getString(R.string.toast_signed_in);
    Toast toast = Toast.makeText(this, text, Toast.LENGTH_LONG);
    toast.show();
    updateMenuOptions();
  }

  private void signOut() {
    String text = getResources().getString(R.string.toast_signing_out);
    Toast toast = Toast.makeText(this, text, Toast.LENGTH_LONG);
    toast.show();
    AuthenticationService.startSignOut(this);
  }

  private void onSignOut() {
    String text = getResources().getString(R.string.toast_signed_out);
    Toast toast = Toast.makeText(this, text, Toast.LENGTH_LONG);
    toast.show();
    updateMenuOptions();
  }

  private void updateMenuOptions() {
    UserManager manager = UserManager.getInstance(this);
    MenuItem signInBtn  = mMenu.findItem(R.id.action_sign_in);
    MenuItem signOutBtn = mMenu.findItem(R.id.action_sign_out);

    if (manager.isAuthenticated()) {
      signInBtn.setVisible(false);
      signOutBtn.setVisible(true);
    } else {
      signInBtn.setVisible(true);
      signOutBtn.setVisible(false);
    }
  }

}
