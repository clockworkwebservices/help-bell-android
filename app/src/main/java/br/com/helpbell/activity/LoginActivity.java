package br.com.helpbell.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import br.com.helpbell.R;
import br.com.helpbell.service.AuthenticationService;
import br.com.helpbell.utils.Constants;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class LoginActivity extends BaseActivity implements View.OnClickListener {

  private static final String TAG = LoginActivity.class.getSimpleName();

  // UI references.
  @InjectView(R.id.email) EditText mEmailView;
  @InjectView(R.id.password) EditText mPasswordView;
  @InjectView(R.id.progress) View mProgressView;
  @InjectView(R.id.login_form) View mFormView;

  BroadcastReceiver mReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      String action = intent.getAction();
      Log.d(TAG, "Broadcast Received - " + action);

      switch (action) {
        case AuthenticationService.ON_SIGN_IN_SUCCESS:
          onSignedIn();
          break;
        case AuthenticationService.ON_SIGN_IN_FAILURE:
          break;
      }
    }
  };

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_login);
    ButterKnife.inject(this);

    mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
      @Override
      public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
        if (id == R.id.login || id == EditorInfo.IME_NULL) {
          signIn();
          return true;
        }
        return false;
      }
    });
  }

  @Override
  protected void onResume() {
    super.onResume();
    IntentFilter intentFilter = new IntentFilter();
    intentFilter.addAction(AuthenticationService.ON_SIGN_IN_SUCCESS);
    intentFilter.addAction(AuthenticationService.ON_SIGN_IN_FAILURE);

    LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver, intentFilter);
  }


  @Override
  protected void onPause() {
    super.onPause();
    LocalBroadcastManager.getInstance(this).unregisterReceiver(mReceiver);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
        finish();
        return true;
    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  @OnClick({ R.id.email_sign_in_button, R.id.forgot_password_button })
  public void onClick(View v) {
    switch (v.getId()) {
      case R.id.email_sign_in_button:
        signIn();
        break;
      case R.id.forgot_password_button:
        startActivityForResult(new Intent(this, NewPasswordActivity.class), Constants.REQUEST_CODE_NEW_PASSWORD);
        break;
    }
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    Log.d(TAG, "onActivityResult - " + requestCode + " - " + resultCode);

    switch (requestCode) {
      case Constants.REQUEST_CODE_NEW_PASSWORD:
        if (resultCode == RESULT_OK) {
          String text = getResources().getString(R.string.toast_new_password);
          Toast toast = Toast.makeText(this, text, Toast.LENGTH_LONG);
          toast.show();
          finish();
        }
        break;
    }
  }

  private void signIn() {
    // Reset errors.
    mEmailView.setError(null);
    mPasswordView.setError(null);

    // Store values at the time of the login attempt.
    String email = mEmailView.getText().toString();
    String password = mPasswordView.getText().toString();

    boolean cancel = false;
    View focusView = null;

    // Check for a valid email address.
    if (TextUtils.isEmpty(email)) {
      mEmailView.setError(getString(R.string.error_email_required));
      focusView = mEmailView;
      cancel = true;
    } else if (!isEmailValid(email)) {
      mEmailView.setError(getString(R.string.error_invalid_email));
      focusView = mEmailView;
      cancel = true;
    } else if (TextUtils.isEmpty(password)) {
      mPasswordView.setError(getString(R.string.error_password_required));
      focusView = mPasswordView;
      cancel = true;
    }

    if (cancel) {
      // There was an error; don't attempt login and focus the first
      // form field with an error.
      focusView.requestFocus();
    } else {
      // Show a progress spinner, and kick off a background task to
      // perform the user login attempt.
      showProgress(true);

      AuthenticationService.startSignIn(this, email, password);
    }
  }

  private boolean isEmailValid(String email) {
    //TODO: Replace this with your own logic
    return email.contains("@");
  }

  private void showProgress(final boolean show) {
    int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

    mFormView.setVisibility(show ? View.GONE : View.VISIBLE);
    mFormView.animate().setDuration(shortAnimTime).alpha(
      show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
      @Override
      public void onAnimationEnd(Animator animation) {
        mFormView.setVisibility(show ? View.GONE : View.VISIBLE);
      }
    });

    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
    mProgressView.animate().setDuration(shortAnimTime).alpha(
      show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
      @Override
      public void onAnimationEnd(Animator animation) {
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
      }
    });
  }

  private void onSignedIn() {
    setResult(RESULT_OK);
    finish();
  }

}

