package br.com.helpbell.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import br.com.helpbell.R;
import br.com.helpbell.model.Device;
import br.com.helpbell.persistance.DeviceManager;
import br.com.helpbell.service.ActivateDeviceService;
import br.com.helpbell.utils.Constants;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class DeviceFoundActivity extends BaseActivity implements View.OnClickListener {

  private static final String TAG = DeviceFoundActivity.class.getSimpleName();

  @InjectView(R.id.device_name) EditText deviceNameView;
  @InjectView(R.id.save_button) Button saveButton;

  private String mDeviceId;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_device_found);
    ButterKnife.inject(this);

    getSupportActionBar().hide();

    Intent intent = getIntent();
    mDeviceId = intent.getStringExtra(Constants.EXTRA_DEVICE_ID);
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    Log.d(TAG, "onDestroy");
  }

  @Override
  @OnClick({ R.id.save_button })
  public void onClick(View v) {
    switch (v.getId()) {
      case R.id.save_button:
        updateDevice();
        break;
    }
  }

  private void updateDevice() {
    // Reset errors.
    deviceNameView.setError(null);
    boolean cancel = false;
    View focusView = null;
    String deviceName = deviceNameView.getText().toString();

    if (TextUtils.isEmpty(deviceName)) {
      deviceNameView.setError(getString(R.string.error_device_name_required));
      focusView = deviceNameView;
      cancel = true;
    }

    if (cancel) {
      focusView.requestFocus();
    } else {
      DeviceManager manager = DeviceManager.getInstance(this);
      Device device = manager.getDevice(mDeviceId);
      device.name = deviceName;
      manager.saveDevice(device);

//      ActivateDeviceService.start(this, mDeviceId);

      onDeviceSaved();
    }
  }

  private void onDeviceSaved() {
    Intent intent = new Intent(this, MainActivity.class);
    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    startActivity(intent);
  }

}
