package br.com.helpbell.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import br.com.helpbell.model.Device;
import br.com.helpbell.model.User;
import br.com.helpbell.persistance.DeviceManager;
import br.com.helpbell.persistance.UserManager;
import br.com.helpbell.rest.ActivateDeviceData;
import br.com.helpbell.rest.HelpBellClient;
import br.com.helpbell.rest.SignInData;
import br.com.helpbell.utils.Constants;
import retrofit.RetrofitError;

public class ActivateDeviceService extends IntentService {

  private static final String TAG = ActivateDeviceService.class.getSimpleName();

  public static final String SIGN_IN_SUCCESS  = "br.com.helpbell.services.ActivateDeviceService.SIGN_IN_SUCCESS";
  public static final String SIGN_IN_FAILURE  = "br.com.helpbell.services.ActivateDeviceService.SIGN_IN_FAILURE";

  public static void start(Context context, String deviceId) {
    Intent intent = new Intent(context, ActivateDeviceService.class);
    intent.putExtra(Constants.EXTRA_DEVICE_ID, deviceId);
    context.startService(intent);
  }

  public ActivateDeviceService() {
    super(TAG);
  }

  @Override
  protected void onHandleIntent(Intent intent) {
    if (intent != null) {
      String deviceId = intent.getStringExtra(Constants.EXTRA_DEVICE_ID);
      DeviceManager manager = DeviceManager.getInstance(this);
      Device device = manager.getDevice(deviceId);
      activateDevice(device);
    }
  }

  private void activateDevice(Device device) {
    HelpBellClient client = new HelpBellClient();
    Intent intent;

    try {
      device = client.getApiService().activateDevice(device.impeeId, new ActivateDeviceData(device.name));

      if (device != null) {
        intent = new Intent(SIGN_IN_SUCCESS);
      } else {
        intent = new Intent(SIGN_IN_FAILURE);
      }
    } catch (RetrofitError e) {
      Log.d(TAG, "Failed to create receiver", e);
//      ACRA.getErrorReporter().handleSilentException(e);
      intent = new Intent(SIGN_IN_FAILURE);
    }

    LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
  }

}
