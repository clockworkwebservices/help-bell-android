package br.com.helpbell.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import br.com.helpbell.model.User;
import br.com.helpbell.persistance.UserManager;
import br.com.helpbell.rest.HelpBellClient;
import br.com.helpbell.rest.Response;
import br.com.helpbell.rest.SignInData;
import br.com.helpbell.utils.Constants;
import retrofit.RetrofitError;


public class NewPasswordService extends IntentService {

  private static final String TAG = NewPasswordService.class.getSimpleName();

  public static final String SIGN_IN_SUCCESS  = "br.com.helpbell.services.NewPasswordService.SIGN_IN_SUCCESS";
  public static final String SIGN_IN_FAILURE  = "br.com.helpbell.services.NewPasswordService.SIGN_IN_FAILURE";

  public static void start(Context context, String email) {
    Intent intent = new Intent(context, NewPasswordService.class);
    intent.putExtra(Constants.EXTRA_USER_EMAIL, email);
    context.startService(intent);
  }

  public NewPasswordService() {
    super(TAG);
  }

  @Override
  protected void onHandleIntent(Intent intent) {
    if (intent != null) {
      User user = new User();
      user.email = intent.getStringExtra(Constants.EXTRA_USER_EMAIL);
      newPassword(new SignInData(user));
    }
  }

  private void newPassword(SignInData data) {
    HelpBellClient client = new HelpBellClient();
    Intent intent;

    try {
      Response response = client.getApiService().newPassword(data);

      if (response != null) {
        intent = new Intent(SIGN_IN_SUCCESS);
      } else {
        intent = new Intent(SIGN_IN_FAILURE);
      }
    } catch (RetrofitError e) {
      Log.d(TAG, "Failed to create receiver", e);
//      ACRA.getErrorReporter().handleSilentException(e);
      intent = new Intent(SIGN_IN_FAILURE);
    }

    LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
  }

}
