package br.com.helpbell.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import br.com.helpbell.model.User;
import br.com.helpbell.persistance.UserManager;
import br.com.helpbell.rest.HelpBellClient;
import br.com.helpbell.rest.SignInData;
import br.com.helpbell.utils.Constants;
import retrofit.RetrofitError;

public class AuthenticationService extends IntentService {

  private static final String TAG = AuthenticationService.class.getSimpleName();

  private static final String ACTION_SIGN_IN  = "br.com.helpbell.services.AuthenticationService.SIGN_IN";
  private static final String ACTION_SIGN_OUT = "br.com.helpbell.services.AuthenticationService.SIGN_OUT";

  public static final String ON_SIGN_IN_SUCCESS  = "br.com.helpbell.services.AuthenticationService.ON_SIGN_IN_SUCCESS";
  public static final String ON_SIGN_IN_FAILURE  = "br.com.helpbell.services.AuthenticationService.ON_SIGN_IN_FAILURE";
  public static final String ON_SIGN_OUT_SUCCESS = "br.com.helpbell.services.AuthenticationService.ON_SIGN_OUT_SUCCESS";
  public static final String ON_SIGN_OUT_FAILURE = "br.com.helpbell.services.AuthenticationService.ON_SIGN_OUT_FAILURE";

  public static void startSignIn(Context context, String email, String password) {
    Intent intent = new Intent(context, AuthenticationService.class);
    intent.setAction(ACTION_SIGN_IN);
    intent.putExtra(Constants.EXTRA_USER_EMAIL, email);
    intent.putExtra(Constants.EXTRA_USER_PASSWORD, password);
    context.startService(intent);
  }

  public static void startSignOut(Context context) {
    Intent intent = new Intent(context, AuthenticationService.class);
    intent.setAction(ACTION_SIGN_OUT);
    context.startService(intent);
  }

  public AuthenticationService() {
    super(TAG);
  }

  @Override
  protected void onHandleIntent(Intent intent) {
    if (intent != null) {
      final String action = intent.getAction();

      if (ACTION_SIGN_IN.equals(action)) {
        User user = new User();
        user.email = intent.getStringExtra(Constants.EXTRA_USER_EMAIL);
        user.password = intent.getStringExtra(Constants.EXTRA_USER_PASSWORD);
        signIn(new SignInData(user));
      } else if (ACTION_SIGN_OUT.equals(action)) {
        signOut();
      }
    }
  }

  private void signIn(SignInData data) {
    HelpBellClient client = new HelpBellClient();
    Intent intent;

    try {
      User user = client.getApiService().signIn(data);

      if (user != null) {
        UserManager manager = UserManager.getInstance(this);
        manager.saveUser(user);
        manager.saveAuthenticationToken(user.authentication_token);

        intent = new Intent(ON_SIGN_IN_SUCCESS);
      } else {
        intent = new Intent(ON_SIGN_IN_FAILURE);
      }
    } catch (RetrofitError e) {
      Log.d(TAG, "Failed to sign in", e);
//      ACRA.getErrorReporter().handleSilentException(e);
      intent = new Intent(ON_SIGN_IN_FAILURE);
    }

    LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
  }

  private void signOut() {
    HelpBellClient client = new HelpBellClient();
    UserManager manager = UserManager.getInstance(this);
    Intent intent;

    try {
      String token = manager.getAuthenticationToken();
      User user = manager.getUser();

      if (user != null && token != null) {
        client.getApiService().signOut(user.email, token);
        manager.saveAuthenticationToken(null);
        manager.saveUser(null);
      }
      intent = new Intent(ON_SIGN_OUT_SUCCESS);
    } catch (RetrofitError e) {
      Log.d(TAG, "Failed to sign out", e);
//      ACRA.getErrorReporter().handleSilentException(e);
      intent = new Intent(ON_SIGN_OUT_FAILURE);
    }

    LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
  }

}
