package br.com.helpbell.persistance;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.google.gson.Gson;

import java.lang.reflect.Type;

public abstract class PersistenceManager {

  protected Context mContext;
  private Gson mGson = new Gson();

  private SharedPreferences mSharedPreferences;

  protected PersistenceManager(Context context) {
    mContext = context;
  }

  protected abstract String getPersistenceFileName();

  private SharedPreferences getSharedPreferences() {
    if (mSharedPreferences == null) {
      mSharedPreferences = mContext.getSharedPreferences(getPersistenceFileName(), 0);
    }
    return mSharedPreferences;
  }

  protected void saveObject(String name, Object value, Type type) {
    String serialized = mGson.toJson(value, type);
    getSharedPreferences().edit().putString(name, serialized).commit();
  }

  protected void saveInt(String name, int value) {
    getSharedPreferences().edit().putInt(name, value).commit();
  }

  protected void saveString(String name, String value) {
    getSharedPreferences().edit().putString(name, value).commit();
  }

  protected void saveBoolean(String name, boolean value) {
    getSharedPreferences().edit().putBoolean(name, value).commit();
  }

  protected Object getObject(String name, Type type) {
    String serialized = getSharedPreferences().getString(name, null);

    if (TextUtils.isEmpty(serialized)) {
      return null;
    }
    return mGson.fromJson(serialized, type);
  }

  protected int getInt(String name) {
    return getSharedPreferences().getInt(name, -1);
  }

  protected String getString(String name) {
    return getSharedPreferences().getString(name, null);
  }

  protected boolean getBoolean(String name) {
    return getSharedPreferences().getBoolean(name, false);
  }

  protected boolean getBoolean(String name, boolean defaultValue) {
    return getSharedPreferences().getBoolean(name, defaultValue);
  }

  protected void deleteValue(String name) {
    getSharedPreferences().edit().remove(name).commit();
  }

}
