package br.com.helpbell.persistance;

import android.content.Context;

import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import br.com.helpbell.model.Device;

public class DeviceManager extends PersistenceManager {

  private static final String DATABASE_NAME     = "devices_db";
  private static final String DATA_DEVICES_NAME = "devices";

  private static DeviceManager sInstance;

  private Type mDevicesType;

  private DeviceManager(Context context) {
    super(context);
    mDevicesType = new TypeToken<Map<String, Device>>(){}.getType();
  }

  public synchronized static DeviceManager getInstance(Context context) {
    if (sInstance == null) {
      sInstance = new DeviceManager(context.getApplicationContext());
    }
    return sInstance;
  }

  public void saveDevices(Map<String, Device> devices) {
    saveObject(DATA_DEVICES_NAME, devices, mDevicesType);
  }

  public Map<String, Device> getDevices() {
    Map<String, Device> devices = (Map<String, Device>) getObject(DATA_DEVICES_NAME, mDevicesType);

    if (devices == null) {
      devices = new HashMap<>();
    }
    return devices;
  }

  public void saveDevice(Device device) {
    Map<String, Device> devices = getDevices();
    devices.put(device.id, device);
    saveDevices(devices);
  }

  public Device getDevice(String id) {
    Map<String, Device> devices = getDevices();
    return devices.get(id);
  }

  public void removeDevice(String id) {
    Map<String, Device> devices = getDevices();
    devices.remove(id);
    saveDevices(devices);
  }

  @Override
  protected String getPersistenceFileName() {
    return DATABASE_NAME;
  }

}
