package br.com.helpbell.persistance;

import android.content.Context;

import br.com.helpbell.model.User;

public class UserManager extends PersistenceManager {

  private static final String DATABASE_NAME             = "users";
  private static final String DATA_OBJECT_NAME          = "user";
  private static final String DATA_AUTHENTICATION_TOKEN = "authentication_token";
  private static final String DATA_GCM_TOKEN            = "gcm_token";

  private static UserManager sInstance;

  private User mUser;
  private String mAuthToken;
  private String mGcmToken;

  private UserManager(Context context) {
    super(context);
  }

  public synchronized static UserManager getInstance(Context context) {
    if (sInstance == null) {
      sInstance = new UserManager(context.getApplicationContext());
    }
    return sInstance;
  }

  public boolean isAuthenticated() {
    return getAuthenticationToken() != null;
  }

  public synchronized User getUser() {
    if (mUser == null) {
      mUser = (User) getObject(DATA_OBJECT_NAME, User.class);
    }
    return mUser;
  }

  public void saveUser(User receiver) {
    mUser = receiver;
    saveObject(DATA_OBJECT_NAME, mUser, User.class);
  }

  public String getAuthenticationToken() {
    if (mAuthToken == null) {
      mAuthToken = getString(DATA_AUTHENTICATION_TOKEN);
    }
    return mAuthToken;
  }

  public void saveAuthenticationToken(String authenticationToken) {
    mAuthToken = authenticationToken;
    saveString(DATA_AUTHENTICATION_TOKEN, mAuthToken);
  }

  public String getGcmToken() {
    if (mGcmToken == null) {
      mGcmToken = getString(DATA_GCM_TOKEN);
    }
    return mGcmToken;
  }

  public void saveGcmToken(String gcmToken) {
    mGcmToken = gcmToken;
    saveString(DATA_GCM_TOKEN, mAuthToken);
  }

  public boolean hasGcmToken() {
    return getGcmToken() != null;
  }

  @Override
  protected String getPersistenceFileName() {
    return DATABASE_NAME;
  }

}
