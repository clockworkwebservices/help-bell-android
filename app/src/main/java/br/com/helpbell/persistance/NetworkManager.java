package br.com.helpbell.persistance;

import android.content.Context;

import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

public class NetworkManager extends PersistenceManager {

  private static final String DATABASE_NAME   = "networks";
  private static final String DATA_WIFIS_NAME = "wifis";

  private static NetworkManager sInstance;

  private Type mWiFisType;

  private NetworkManager(Context context) {
    super(context);
    mWiFisType = new TypeToken<Map<String, String>>(){}.getType();
  }

  public synchronized static NetworkManager getInstance(Context context) {
    if (sInstance == null) {
      sInstance = new NetworkManager(context.getApplicationContext());
    }
    return sInstance;
  }

  public void saveWiFiPassword(String ssid, String password) {
    Map<String, String> wifis = getWiFis();
    wifis.put(ssid, password);
    saveWiFis(wifis);
  }

  public String getWifiPassword(String ssid) {
    return getWiFis().get(ssid);
  }

  public void removeWifiPassword(String ssid) {
    removeWiFi(ssid);
  }

  private Map<String, String> getWiFis() {
    Map<String, String> wifis = (Map<String, String>) getObject(DATA_WIFIS_NAME, mWiFisType);

    if (wifis == null) {
      wifis = new HashMap<>();
    }
    return wifis;
  }

  private void removeWiFi(String ssid) {
    Map<String, String> wifis = getWiFis();
    wifis.remove(ssid);
    saveWiFis(wifis);
  }

  private void saveWiFis(Map<String, String> wifis) {
    saveObject(DATA_WIFIS_NAME, wifis, mWiFisType);
  }

  @Override
  protected String getPersistenceFileName() {
    return DATABASE_NAME;
  }

}
