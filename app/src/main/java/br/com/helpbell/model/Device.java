package br.com.helpbell.model;

import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Device {

  public String name;

  public String id;
  public String impeeId;
  public String planId;
  public String impId;
  public String claimedAt;
  public String agentUrl;
  public String eiMessageId;

  private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale.US);

  public static Device fromJsonObject(JSONObject json) throws JSONException {
    Gson gson = new Gson();
    Log.d(Device.class.getSimpleName(), gson.toJson(json));

    Device device = new Device();
    device.id        = json.getString("id");
    device.impeeId   = json.getString("impee_id");
    device.planId    = json.getString("plan_id");
    device.impId     = json.getString("imp_id");
    device.claimedAt = json.getString("claimed_at").replace("Z", "+0:00");
    device.agentUrl  = json.getString("agent_url");
    device.eiMessageId = json.getString("ei_message_id");

    if (device.impeeId != null) {
      device.impeeId = device.impeeId.trim();
    }

    return device;
  }

}
