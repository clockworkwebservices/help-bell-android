package br.com.helpbell.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;

import br.com.helpbell.utils.Constants;

public class MessageDialogFragment
  extends DialogFragment
  implements DialogInterface.OnClickListener {


  private OnFragmentInteractionListener mListener;

  public static MessageDialogFragment newInstance(
    String title, String message, String positiveButtonLabel, String negativeButtonLabel, int requestCode
  ) {
    MessageDialogFragment dialogFragment = new MessageDialogFragment();
    Bundle args = new Bundle();
    args.putString(Constants.EXTRA_TITLE, title);
    args.putString(Constants.EXTRA_MESSAGE, message);
    args.putString(Constants.EXTRA_POSITIVE_BUTTON_LABEL, positiveButtonLabel);
    args.putString(Constants.EXTRA_NEGATIVE_BUTTON_LABEL, negativeButtonLabel);
    args.putInt(Constants.EXTRA_REQUEST_CODE, requestCode);
    dialogFragment.setArguments(args);
    return dialogFragment;
  }

  @Override
  public void onAttach(Activity activity) {
    super.onAttach(activity);

    if (activity instanceof OnFragmentInteractionListener) {
      mListener = (OnFragmentInteractionListener) activity;
    }
  }

  @NonNull
  @Override
  public Dialog onCreateDialog(Bundle savedInstanceState) {
    String title = getArguments().getString(Constants.EXTRA_TITLE);
    String message = getArguments().getString(Constants.EXTRA_MESSAGE);
    String positiveButtonLabel = getArguments().getString(Constants.EXTRA_POSITIVE_BUTTON_LABEL);
    String negativeButtonLabel = getArguments().getString(Constants.EXTRA_NEGATIVE_BUTTON_LABEL);
//    TextView title = new TextView(getActivity());
//    title.setText(getArguments().getString(ARG_TITLE));
//    title.setPadding(20, 20, 20, 20);
//    title.setGravity(Gravity.CENTER);

    setCancelable(false);

    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
//      .setCustomTitle(title)
      .setTitle(title)
      .setMessage(message)
      .setPositiveButton(positiveButtonLabel, MessageDialogFragment.this);

    if (negativeButtonLabel != null) {
      builder.setNegativeButton(negativeButtonLabel, MessageDialogFragment.this);
    }
    return builder.create();
  }

  @Override
  public void onDetach() {
    super.onDetach();

    if (mListener != null) {
      mListener = null;
    }
  }

  @Override
  public void onClick(DialogInterface dialog, int which) {
    int requestCode = getArguments().getInt(Constants.EXTRA_REQUEST_CODE);

    switch (which) {
      case DialogInterface.BUTTON_POSITIVE:
        if (mListener != null) {
          mListener.onPositiveButtonClick(requestCode);
        }
        break;
      case DialogInterface.BUTTON_NEGATIVE:
        if (mListener != null) {
          mListener.onNegativeButtonClick(requestCode);
        }
        break;
    }
  }

  public interface OnFragmentInteractionListener {
    void onPositiveButtonClick(int requestCode);
    void onNegativeButtonClick(int requestCode);
  }


}
