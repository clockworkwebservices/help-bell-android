package br.com.helpbell.fragment;

import android.app.DialogFragment;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;

import br.com.helpbell.utils.Constants;

public class ProgressDialogFragment extends DialogFragment {


  public static ProgressDialogFragment newInstance(String message) {
    ProgressDialogFragment dialogFragment = new ProgressDialogFragment();
    Bundle args = new Bundle();
    args.putString(Constants.EXTRA_MESSAGE, message);
    dialogFragment.setArguments(args);
    return dialogFragment;
  }


  @Override
  public Dialog onCreateDialog(Bundle savedInstanceState) {
    String message = getArguments().getString(Constants.EXTRA_MESSAGE);
//    setCancelable(false);
    return ProgressDialog.show(getActivity(), null, message, true, false);
  }


}
