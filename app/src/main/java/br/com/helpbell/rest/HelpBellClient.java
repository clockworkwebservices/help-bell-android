package br.com.helpbell.rest;

import com.google.gson.Gson;

import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

public class HelpBellClient {

  public static final String BASE_URL = "https://helpbell.com.br";

  private HelpBellServices mApiService;

  public HelpBellClient() {
    Gson gson = new Gson();

    RestAdapter restAdapter = new RestAdapter.Builder()
      .setLogLevel(RestAdapter.LogLevel.FULL)
      .setEndpoint(BASE_URL)
      .setConverter(new GsonConverter(gson))
      .setRequestInterceptor(new HelpBellRequestInterceptor())
      .build();

    mApiService = restAdapter.create(HelpBellServices.class);
  }


  public HelpBellServices getApiService() {
    return mApiService;
  }


}
