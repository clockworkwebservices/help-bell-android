package br.com.helpbell.rest;

import br.com.helpbell.model.Device;
import br.com.helpbell.model.User;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.Header;
import retrofit.http.Headers;
import retrofit.http.PATCH;
import retrofit.http.POST;
import retrofit.http.Path;

public interface HelpBellServices {

  @Headers({
    "Content-type: application/json",
    "Accept: application/json"
  })
  @POST("/auth/sign_in")
  public User signIn(@Body SignInData data);

  @Headers({
    "Content-type: application/json",
    "Accept: application/json"
  })
  @DELETE("/auth/sign_out")
  public User signOut(@Header("X-User-Email") String email, @Header("X-User-Token") String token);

  @Headers({
    "Content-type: application/json",
    "Accept: application/json"
  })
  @POST("/auth/password")
  public Response newPassword(@Body SignInData data);

  @Headers({
    "Content-type: application/json",
    "Accept: application/json"
  })
  @PATCH("/devices/{id}/active")
  public Device activateDevice(@Path("id") String deviceId, @Body ActivateDeviceData data);

}
